#!/bin/sh

# the maximum number of strings to shuffle/sort
if test -n "$1"
then
    MAX_NUM="$1"
else
    MAX_NUM=1024
fi

for n in `seq 1 $MAX_NUM`
do got=`seq 1 $n | ./shuffle -r /dev/urandom | sort -n | uniq | wc -l`
   if [ "$got" != "$n" ]
   then echo "expected $n, obtained ${got} instead"
        exit 1
   fi
done
