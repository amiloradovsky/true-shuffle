#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sysexits.h>
#include <getopt.h>

const size_t DEFAULT_PERMUTATION_SIZE = 1 << 16;
const size_t DEFAULT_BUFFER_SIZE = 1 << 20;
const char * DEFAULT_DELIMITERS = "\n\r";  /* input */
const char * DEFAULT_MINOR_SEPARATOR = "\n";  /* output, within block */
const char * DEFAULT_MAJOR_SEPARATOR = "\n";  /* output, after block */

int is_eof = 0 ;  /* assume it isn't, by default */

void
die (char * message, int code)
{
  err (code, "error: %s! exiting...\n", message);
}

void
attention (char * message)
{
  warn ("warning: %s. anyway...\n", message);
}

char
read_char ()
{
  char c ;
  int have_read = read (STDIN_FILENO, &c, 1);
  if (have_read == 0) is_eof ++ ;
  else if (have_read != 1)
    die ("couldn't read the input", 1);

  return c ;
}

int
read_line (const char * dels, char * buffer, u_int length)
  {
    char * current = buffer ;

    while (length != 0)
      {
        char c = read_char ();
        if (is_eof) goto eof;  /* nothing */
        if (c == 0 || strchr (dels, c))
          {
            * current = 0, current ++ ;
            return (current - buffer);
          }
        else
          {
            * current = c, current ++ ;
            length -- ;
          }
      }

    die ("the input doesn't fit into the buffer", 2);

  eof:
    return 0;
  }

u_int
read_strings (const char * delimiters,
              char * buffer, u_int buffer_size,
              char ** strings, u_int permutation_size)
{
  u_int current = 0 ;
  size_t free = buffer_size;
  size_t length;
  while (0 == 0)
    {
      if (current == permutation_size)
        break;
      strings [current] = (current == 0) ?
        buffer : strings [current - 1] + length ;
      if ((length = read_line (delimiters,
                               strings [current],
                               free)) == 0)
        break;
      free -= length ;
      current ++ ;
    }

  return current ;
}

void
print_strings (const char * minor_separator, const char * major_separator,
               char ** strings, const u_int * permutation, u_int n)
{
  u_int i;

  for (i = 0; i != n; i ++)
    printf ("%s%s", strings [permutation [i]],
            (i + 1 == n) ? major_separator : minor_separator);
}

void
seed_obtain (const char * seed_file)
{
#ifdef OPENBSD
    srandomdev();
#else
    int sfd = open (seed_file, O_RDONLY);
    unsigned int seed ;
    if (read (sfd, &seed, sizeof (seed)) != sizeof (seed))
      die ("couldn't read the seed in full", 1);
    close (sfd);
    srandom (seed);
#endif
}

u_int *
unitary_permutation (u_int * perm, u_int n)
{
  u_int i;

  for (i = 0; i != n; i ++) perm [i] = i;

  return perm;
}

u_int *
randomize_permutation (u_int * perm, u_int n)
{
  u_int i;

  /* does nothing for i = 1, but n = 0 is possible */
  for (i = n; i != 0; i --)
    {
      /* choose an index from [0, i - 1] */
      u_int k = random () / (RAND_MAX / i + 1);
      /* then swap these two values */
      u_int t = perm [k]; perm [k] = perm [i - 1]; perm [i - 1] = t;
    }

  return perm;
}

int
main( int argc, char ** argv )
  {
    /* parse the options */
    size_t permutation_size = DEFAULT_PERMUTATION_SIZE;
    size_t buffer_size = DEFAULT_BUFFER_SIZE;
    const char * delimiters = DEFAULT_DELIMITERS;
    const char * minor_separator = DEFAULT_MINOR_SEPARATOR;
    const char * major_separator = DEFAULT_MAJOR_SEPARATOR;
    const char * seed_file = "/dev/random";  /* blocking, if overused */
    for (int opt; (opt = getopt (argc, argv, "r:n:l:d:s:t:")) != -1; )
      switch ((char) opt)
        {
        case 'r':
          seed_file = optarg;
          break;
        case 'n':
          permutation_size = atoi (optarg);
          if (permutation_size == 0)
            die ("zero permutation size will lead to an infinite loop", 1);
          if (permutation_size == 1)
            attention ("the behavior is basically as that of `cat`");
          break;
        case 'l':
          buffer_size = atoi (optarg);
          break;
        case 'd':
          delimiters = optarg;
          break;
        case 's':
          minor_separator = optarg;
          break;
        case 't':
          major_separator = optarg;
          break;
        }

    /* prepare a (truly) random seed */
    seed_obtain (seed_file);

    /* prepare the buffers */
    char * buffer = malloc (buffer_size);
    char ** strings = calloc (permutation_size, sizeof (char *));
    u_int * permutation = calloc (permutation_size, sizeof (u_int));

    u_int c;
    do
      {
        /* read the array of strings */
        c = read_strings (delimiters,
                          buffer, buffer_size,
                          strings, permutation_size);

        /* prepare the random permutation */
        permutation = unitary_permutation (permutation, c);
        permutation = randomize_permutation (permutation, c);

        /* print the resulting permutation of the input lines */
        print_strings (minor_separator, major_separator,
                       strings, permutation, c);
      }
    while (c == permutation_size);
    /* we simply ran out of permutation slots -- repeat */

    return EX_OK;
  }
