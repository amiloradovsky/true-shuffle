
prefix = /usr
bindir = $(prefix)/bin

INSTALL = install
INSTALL_STRIP = -s
INSTALL_DIR = $(INSTALL) -d -m 0755
INSTALL_PROGRAM = $(INSTALL) -m 0755 $(INSTALL_STRIP)

CFLAGS += -O3 -Werror #-DOPENBSD
# OpenBSD has a special function to set the seed from the device.
# In order to use that method, uncomment the last option above.
# (It will render the `-r` option useless though.)
CPPFLAGS += -O3 -Werror

shuffle : shuffle.c
	${CC} ${LDFLAGS} ${CFLAGS} -o $@ $<
shuffle.new : shuffle.cpp
	${CXX} ${LDFLAGS} ${CPPFLAGS} -o $@ $<

all : shuffle shuffle.new

install : shuffle
	$(INSTALL_DIR) $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) shuffle $(DESTDIR)$(bindir)/

uninstall :
	rm -f $(DESTDIR)$(bindir)/shuffle

clean :
	rm -f shuffle shuffle.new
