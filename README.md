Performs random permutations on the input lines (block-wise),
separated by zeros or an additional set of characters.

Usage:
--------

    shuffle -r <seed file>
            -n <lines>
            -l <buffer>
            -d <input delimiters>
            -s <minor output separator>
            -t <major output separator>

The lines are permuted within blocks, of size defined by `-n`.
All the text contained in those lines has to fit the buffer,
the size of which is defined by `-l`. The reason for that,
is because *all* the data have to be read into the memory *before* the printing,
since the *order* is not known in advance.
So if the buffer is too small to store all the strings/lines within a block,
the program will just fail -- not very convenient but accurate.

To avoid that behavior either the number of lines has to be smaller
(the limiting case is one line per block, a la `cat`, but still no guarantee),
or the buffer has to be larger.

The seed file is by default `/dev/random`, which is high-quality entropy source,
but yet it's blocking -- when called often (say, in scripts), override it.

Examples:
--------

Generate a list of unique numbers (from 1 to 16 inclusive) in an order

    seq 1 16 | shuffle

Check if it preserves the set, only altering the order

    seq 1 65536 | shuffle -n 16 | sort -n | uniq | wc -l

Shuffle files from the current directory

    ls -1 | shuffle

Pick any four

    ls -1 | shuffle | head -4

Same but with a tree

    find /path/to/somewhere | shuffle | head -4 | xargs file

Or, for the fun of it

    echo "one two three four " | ./shuffle -d " " -n 2 -s " and "

and

    echo -e "red\nlight\n\0green\nlight\n\0" | ./shuffle -d "" -s "" -t ""

To-do:
--------

- at the moment it reads data synchronously (character by character)
-- that's not very efficient. (On the other hand it is just 200 LoC.)
