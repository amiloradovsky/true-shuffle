#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sysexits.h>
#include <getopt.h>

#include <iostream>
#include <string>
#include <random>

using namespace std;

const size_t DEFAULT_PERMUTATION_SIZE = 1 << 16;
const size_t DEFAULT_BUFFER_SIZE = 1 << 20;
const string DEFAULT_DELIMITERS = "\n\r";  /* input */
const string DEFAULT_MINOR_SEPARATOR = "\n";  /* output, within block */
const string DEFAULT_MAJOR_SEPARATOR = "\n";  /* output, after block */

int is_eof = 0 ;  /* assume it isn't, by default */

void
die (string message, int code)
{
  err (code, "error: %s! exiting...\n", message.c_str ());
}

void
attention (string message)
{
  warn ("warning: %s. anyway...\n", message.c_str ());
}

char
read_char ()
{
  char c ;
  int have_read = read (STDIN_FILENO, &c, 1);
  if (have_read == 0) is_eof ++ ;
  else if (have_read != 1)
    die ("couldn't read the input", 1);

  return c ;
}

int
read_line (const string dels, char * buffer, u_int length)
  {
    char * current = buffer ;

    while (length != 0)
      {
        char c = read_char ();
        if (is_eof) goto eof;  /* nothing */
        if (c == 0 || strchr (dels.c_str (), c))
          {
            * current = 0, current ++ ;
            return (current - buffer);
          }
        else
          {
            * current = c, current ++ ;
            length -- ;
          }
      }

    die ("the input doesn't fit into the buffer", 2);

  eof:
    return 0;
  }

u_int
read_strings (const string delimiters,
              char * buffer, u_int buffer_size,
              char ** strings, u_int permutation_size)
{
  u_int current = 0 ;
  size_t free = buffer_size;
  size_t length;
  while (0 == 0)
    {
      if (current == permutation_size)
        break;
      strings [current] = (current == 0) ?
        buffer : strings [current - 1] + length ;
      if ((length = read_line (delimiters,
                               strings [current],
                               free)) == 0)
        break;
      free -= length ;
      current ++ ;
    }

  return current ;
}

void
print_strings (const string minor_separator,
               const string major_separator,
               char ** strings, const u_int * permutation, u_int n)
{
  u_int i;

  for (i = 0; i != n; i ++)
    printf ("%s%s", strings [permutation [i]],
            (i + 1 == n) ? major_separator.c_str ()
                         : minor_separator.c_str ());
}

u_int *
unitary_permutation (u_int * perm, u_int n)
{
  u_int i;

  for (i = 0; i != n; i ++) perm [i] = i;

  return perm;
}

u_int *
randomize_permutation (mt19937& rng, u_int * perm, u_int n)
{
  for (u_int i = n; i != 0; i --)
    {
      uniform_int_distribution<mt19937::result_type>
        distribution (0, i - 1);
      u_int k = distribution (rng);
      u_int t = perm [k]; perm [k] = perm [i - 1]; perm [i - 1] = t;
      /* swap these two values/entries */
    }

  return perm;
}

int
main( int argc, char ** argv )
  {
    /* parse the options */
    size_t permutation_size = DEFAULT_PERMUTATION_SIZE;
    size_t buffer_size = DEFAULT_BUFFER_SIZE;
    string delimiters = DEFAULT_DELIMITERS;
    string minor_separator = DEFAULT_MINOR_SEPARATOR;
    string major_separator = DEFAULT_MAJOR_SEPARATOR;
    for (int opt; (opt = getopt (argc, argv, "n:l:d:s:t:")) != -1; )
      switch ((char) opt)
        {
        case 'n':
          permutation_size = atoi (optarg);
          if (permutation_size == 0)
            die ("zero permutation size will lead to an infinite loop", 1);
          if (permutation_size == 1)
            attention ("the behavior is basically as that of `cat`");
          break;
        case 'l':
          buffer_size = atoi (optarg);
          break;
        case 'd':
          delimiters = optarg;
          break;
        case 's':
          minor_separator = optarg;
          break;
        case 't':
          major_separator = optarg;
          break;
        }

    /* prepare a (truly) random seed & generator */
    mt19937 rng;
    rng.seed (random_device () ());

    /* prepare the buffers */
    char * buffer = new char [buffer_size];
    char ** strings = (char **) calloc (permutation_size,
                                        sizeof (char *));
    u_int * permutation = new u_int [permutation_size];

    u_int c;
    do
      {
        /* read the array of strings */
        c = read_strings (delimiters,
                          buffer, buffer_size,
                          strings, permutation_size);

        /* prepare the random permutation */
        permutation = unitary_permutation (permutation, c);
        permutation = randomize_permutation (rng, permutation, c);

        /* print the resulting permutation of the input lines */
        print_strings (minor_separator, major_separator,
                       strings, permutation, c);
      }
    while (c == permutation_size);
    /* we simply ran out of permutation slots -- repeat */

    return EX_OK;
  }
